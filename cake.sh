#!/usr/bin/env bash

frame0=$(cat << EOF
                0   0
                |   |
            ____|___|____
         0  |~ ~ ~ ~ ~ ~|   0
         |  |           |   |
      ___|__|___________|___|__
      |/\/\/\/\/\/\/\/\/\/\/\/|
  0   |       H a p p y       |   0
  |   |/\/\/\/\/\/\/\/\/\/\/\/|   |
 _|___|_______________________|___|__
|/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/|
|                                   |
|         B i r t h d a y! ! !      |
| ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ |
|___________________________________|
EOF
)

frame1=$(cat << EOF
                1   1
                |   |
            ____|___|____
         1  |~ ~ ~ ~ ~ ~|   1
         |  |           |   |
      ___|__|___________|___|__
      |/\/\/\/\/\/\/\/\/\/\/\/|
  1   |       H a p p y       |   1
  |   |/\/\/\/\/\/\/\/\/\/\/\/|   |
 _|___|_______________________|___|__
|/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/|
|                                   |
|         B i r t h d a y! ! !      |
| ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ |
|___________________________________|
EOF
)

frame_width=37
frame_height=15

terminal_size() {
       	# Calculate the size of the terminal
	terminal_cols="$(tput cols)"
	terminal_rows="$(tput lines)"
}

plot_frame() {
	width=$((terminal_cols/2 - $frame_width/2))
	while read -r
	do
		tput cuf $width
		echo "$REPLY"
	done <<< "$1"
}

# Set a trap to restore terminal on exit.
# - reset character attributes,
# - make cursor visible, and
# - restore previous screen contents
trap 'exit 0' SIGINT
trap 'tput sgr0; tput cnorm; tput rmcup || clear' EXIT


# Save screen contents and make cursor invisible
tput smcup; tput civis

# Start work
terminal_size
height=$((terminal_rows/2 - $frame_height/2))

for i in `seq ${1-10}`
do
	tput cup $height 0
	plot_frame "$frame0"
	sleep 1
	tput cup $height 0
	plot_frame "$frame1"
	sleep 1
done
